package game.mechanics;

import game.board.Board;
import game.board.pieces.Piece;
import game.players.Player;

public class ScoreManager {
	private static ScoreManager instance;

	public static ScoreManager getInstance() {
		if (instance == null) {
			instance = new ScoreManager();
		}
		return instance;
	}

	/*
	 * Checks if there is any winner. Should be called before each turn, as a
	 * conditional statement for player's turn. Returns true if there is a
	 * winner, else returns false.
	 * 
	 * 
	 * @Param - board of current game
	 *
	 */
	public boolean checkForWinner(Board board, Player player1, Player player2) {
		if (player1.getOwnMovablePieces().size() == 0) {
			anounceWinner(player2);
			return true;
		}
		if (player2.getOwnMovablePieces().size() == 0) {
			anounceWinner(player1);
			return true;
		}
		// TODO check if there are any white on fields in row 8 or black in row
		for (char x = 'a'; x <= 'h'; x = (char) (x + 1)) {
			if (!board.getField(x, '1').checkIfEmpty()) {
				if (board.getField(x, '1').getPiece().getPieceType() == Piece.Type.BLACK) {
					anounceWinner(player2);
					return true;
				}
			}
			if (!board.getField(x, '8').checkIfEmpty()) {
				if (board.getField(x, '8').getPiece().getPieceType() == Piece.Type.WHITE) {
					anounceWinner(player1);
					return true;
				}
			}
		}
		return false;
	}

	public void anounceWinner(Player player) {
		System.out.println();
		System.out.println("Winner is " + player.getName());
	}
}