package game.mechanics;

import game.players.Player;

public class Settings {
	private static Player PLAYER1;
	private static Player PLAYER2;

	public static Player getPLAYER1() {
		return PLAYER1;
	}

	public static void setPLAYER1(Player player1) {
		PLAYER1 = player1;
	}

	public static Player getPLAYER2() {
		return PLAYER2;
	}

	public static void setPLAYER2(Player player2) {
		PLAYER2 = player2;
	}
}
