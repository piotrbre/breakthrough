package game.mechanics;

import java.util.ArrayList;

import game.board.Board;
import game.board.pieces.Piece;
import game.players.Player;

public class MoveManager {

	private static MoveManager instance;

	private MoveManager() {
	}

	public static MoveManager getInstance() {
		if (instance == null) {
			instance = new MoveManager();
		}
		return instance;
	}

	/*
	 * Attempts a move of piece from field with start coordinates, to field with
	 * destination coordinates. If move is invalid due to violation of game
	 * rules, returns false. If move proves itself to be valid, move is applied
	 * to board with applyMove(...).
	 * 
	 * @Param start - coordinates of piece to be moved (partial validation
	 * required: they must match actual field on the board)
	 * 
	 * @Param destination - same as above, but destination
	 */
	public boolean attemptMove(char[] start, char[] destination, Piece.Type pieceType, Board board, Player current) {
		if (checkMoveValidity(start, destination, pieceType, board)) {
			applyMove(start, destination, pieceType, board, current);
			return true;
		} else {
			return false;
		}
	}

	// TODO: Checks for compliance of move with game rules
	private boolean checkMoveValidity(char[] start, char[] goal, Piece.Type pieceType, Board board) {
		if (startHasPlayersPiece(start, pieceType, board)) {
			if (isPossibleMove(start, goal, pieceType, board)) {
				return true;
			}
		}
		return false;
	}

	public boolean isPossibleMove(char[] start, char[] goal, Piece.Type pieceType, Board board) {
		ArrayList<char[]> possibleDestinations = mapPossibleDestinations(start, pieceType, board);
		if (checkIfCoordsPresent(goal, possibleDestinations)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean checkIfCoordsPresent(char[] goal, ArrayList<char[]> possibleDestinations) {
		for (char[] current : possibleDestinations) {
			if (current[0] == goal[0]) {
				if (current[1] == goal[1]) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean startHasPlayersPiece(char[] start, Piece.Type pieceType, Board board) {
		if (board.getField(start[0], start[1]).checkIfEmpty()) {
			return false;
		} else if (!(board.getField(start[0], start[1]).getPiece().getPieceType() == pieceType)) {
			return false;
		}
		return true;
	}

	public boolean areCoords(char ch[], int x, int y) {
		if (ch[x] >= 'a' && ch[x] <= 'h') {
			if (ch[y] >= '1' && ch[y] <= '8') {
				return true;
			}
		}
		return false;
	}

	public ArrayList<char[]> mapPossibleDestinations(char[] start, Piece.Type pieceType, Board board) {
		ArrayList<char[]> possibleDestinations = new ArrayList<char[]>();
		char[] diagLeftCoords;
		char[] diagRightCoords;
		char[] straightCoords;
		if (pieceType == Piece.Type.BLACK) {
			diagLeftCoords = new char[] { (char) (start[0] - 1), (char) (start[1] - 1) };
			straightCoords = new char[] { (char) (start[0]), (char) (start[1] - 1) };
			diagRightCoords = new char[] { (char) (start[0] + 1), (char) (start[1] - 1) };
		} else {
			diagLeftCoords = new char[] { (char) (start[0] - 1), (char) (start[1] + 1) };
			straightCoords = new char[] { (char) (start[0]), (char) (start[1] + 1) };
			diagRightCoords = new char[] { (char) (start[0] + 1), (char) (start[1] + 1) };
		}
		if (board.getField(diagLeftCoords[0], diagLeftCoords[1]).checkIfEmpty()) {
			possibleDestinations.add(diagLeftCoords);
		} else {
			if (((board.getField(diagLeftCoords[0], diagLeftCoords[1]).getPiece().getPieceType()) != pieceType)
					&& (board.getField(diagLeftCoords[0], diagLeftCoords[1]).getPiece()
							.getPieceType() != Piece.Type.BORDER)) {
				possibleDestinations.add(diagLeftCoords);
			}
		}
		if (board.getField(diagRightCoords[0], diagRightCoords[1]).checkIfEmpty()) {
			possibleDestinations.add(diagRightCoords);
		} else {
			if (((board.getField(diagRightCoords[0], diagRightCoords[1]).getPiece().getPieceType()) != pieceType)
					&& (board.getField(diagRightCoords[0], diagRightCoords[1]).getPiece()
							.getPieceType() != Piece.Type.BORDER)) {
				possibleDestinations.add(diagRightCoords);
			}
		}
		if (board.getField(straightCoords[0], straightCoords[1]).checkIfEmpty()) {
			possibleDestinations.add(straightCoords);
		}
		return possibleDestinations;
	}

	public void applyMove(char[] start, char[] destination, Piece.Type pieceType, Board board, Player current) {
		board.getField(start[0], start[1]).setPiece(null);
		board.getField(destination[0], destination[1]).setPiece(new Piece(pieceType));
		board.drawBoard();
		System.out.println();
		System.out.print(current.getName() + ": " + start[0] + start[1] + " -> " + destination[0] + destination[1]);
	}
}
