package game.board.pieces;

public class Piece {
	/*
	 * PieceType states possible types of pieces. BORDER is special PieceType
	 * created exclusively for purpose of occupation of the outermost,
	 * inaccessible fields on board. Board border mechanism improves clarity of
	 * EasyAIPlayer and TrivialAIPlayer move-making algorithms.
	 */
	public enum Type {
		BLACK, WHITE, BORDER;
	}

	public Piece(Type pieceType) {
		this.pieceType = pieceType;
	}

	private Type pieceType;

	public Type getPieceType() {
		return pieceType;
	}

	public void setPieceType(Type pieceType) {
		this.pieceType = pieceType;
	}
}
