package game.board;

import game.board.pieces.Piece;

public class Field {
	public Field() {

	}

	public Field(char x, char y) {
		if (((y == '1') || (y == '2')) && (((x != '_') && (x != '`')) && ((x != 'i') && (x != 'j')))) {
			this.piece = new Piece(Piece.Type.WHITE);
		} else if (((y == '7') || (y == '8')) && (((x != '_') && (x != '`')) && ((x != 'i') && (x != 'j')))) {
			this.piece = new Piece(Piece.Type.BLACK);
		} else if ((y == '0') || (y == ':')) {
			this.piece = new Piece(Piece.Type.BORDER);
		} else if ((((x == '_') || (x == '`')) || ((x == 'i') || (x == 'j'))) || (((y == '/') || (y == '0')))
				|| ((y == '9') || (y == ':'))) {
			this.piece = new Piece(Piece.Type.BORDER);
		} else {
			this.piece = null;
		}
	}

	/*
	 * Piece that is currently on the field
	 */
	private Piece piece;

	public Piece getPiece() {
		return piece;
	}

	public void setPiece(Piece piece) {
		this.piece = piece;
	}

	/*
	 * Returns true if field is empty
	 */
	public boolean checkIfEmpty() {
		if (this.piece == null) {
			return true;
		} else {
			return false;
		}
	}

}
