package game.board;

import java.util.HashMap;
import java.util.Map;

import game.board.pieces.Piece;

public class Board {

	private Map<Character, Integer> xMap;
	private Map<Character, Integer> yMap;
	private static Board instance;

	public Map<Character, Integer> getxMap() {
		return xMap;
	}

	public void setxMap(Map<Character, Integer> xMap) {
		this.xMap = xMap;
	}

	public Map<Character, Integer> getyMap() {
		return yMap;
	}

	public void setyMap(Map<Character, Integer> yMap) {
		this.yMap = yMap;
	}

	public Board() {
		xMap = new HashMap<Character, Integer>();
		int i = 0;
		for (char x = '_'; x < 'k'; x = (char) (x + 1), i++) {
			xMap.put(x, i);
		}
		yMap = new HashMap<Character, Integer>();
		int j = 0;
		for (char y = ':'; y > '.'; y = (char) (y - 1), j++) {
			yMap.put(y, j);
		}
		fields = new Field[12][12];
		int ix = 0;
		for (char x = '_'; x < 'k'; x = ((char) (x + 1)), ix++) {
			int iy = 0;
			for (char y = ':'; y > '.'; y--, iy++) {
				fields[ix][iy] = new Field(x, y);
			}
		}
	}

	public static Board getInstance() {
		if (instance == null) {
			instance = new Board();
		}
		return instance;
	}

	private Field[][] fields;

	public Field getField(char x, char y) {
		return fields[this.xMap.get(x)][this.yMap.get(y)];
	}

	public void drawBoard() {
		System.out.println();
		for (char y = ':'; y > '.'; y = (char) (y - 1)) {
			System.out.println("");
			for (char x = '_'; x < 'k'; x = (char) (x + 1)) {
				if (this.getField(x, y).checkIfEmpty()) {
					System.out.print(" ");
				} else {
					if (this.getField(x, y).getPiece().getPieceType() == Piece.Type.BLACK) {
						System.out.print("#");
					}
					if (this.getField(x, y).getPiece().getPieceType() == Piece.Type.WHITE) {
						System.out.print("@");
					}
					if (this.getField(x, y).getPiece().getPieceType() == Piece.Type.BORDER) {
						System.out.print(".");
					}
				}
			}
		}
	}

}
