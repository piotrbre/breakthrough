package game;

import game.board.Board;
import game.mechanics.MoveManager;
import game.mechanics.ScoreManager;
import game.mechanics.Settings;
import game.players.Player;

public class Game {
	Board board;
	MoveManager moveManager;
	Player player1;
	Player player2;
	ScoreManager scoreManager;
	Settings settings;

	public Game(Player player1, Player player2) {
		this.player1 = player1;
		this.player2 = player2;
		this.moveManager = MoveManager.getInstance();
		this.scoreManager = ScoreManager.getInstance();
	}

	public void initialize() {
		board = new Board();
		updatePlayersPiecesStatus();
	}

	public void goOn() {
		updatePlayersPiecesStatus();
		this.board.drawBoard();
		while (!this.scoreManager.checkForWinner(this.board, this.player1, this.player2)) {
			player1.playOwnTurn(board, this.moveManager);
			player2.playOwnTurn(board, this.moveManager);
			updatePlayersPiecesStatus();
		}
	}

	/*
	 * Updates lists of both player's pieces and their movable pieces. This
	 * method has to be called at the end of turn, before turn is over and
	 * before checkForWinners gets called)
	 */
	void updatePlayersPiecesStatus() {
		player1.listOwnPieces(this.board);
		player1.listOwnMovablePieces(this.board);
		player2.listOwnPieces(this.board);
		player2.listOwnMovablePieces(this.board);
	}

}
