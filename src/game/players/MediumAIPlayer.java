package game.players;

import java.util.ArrayList;

import game.board.Board;
import game.board.pieces.Piece;
import game.board.pieces.Piece.Type;
import game.mechanics.MoveManager;

public class MediumAIPlayer extends AIPlayer {

	ArrayList<char[]> possibleAttackers = new ArrayList<char[]>();

	public MediumAIPlayer(String playersName, Type pieceType) {
		super(playersName, pieceType);
		// TODO Auto-generated constructor stub
	}

	@Override
	public char[] pickDestination(char[] start, MoveManager moveManager, Board board) {
		if (possibleAttackers.size() == 0) {
			return pickRandomDestination(start, moveManager, board);
		} else
			return pickRandomAttackDestination(start, board);
	}

	@Override
	public char[] pickStart(Board board) {
		listPossibleAttackers(board);
		if (possibleAttackers.size() == 0) {
			return pickRandomCoordinates(getOwnMovablePieces());
		} else {
			return pickRandomCoordinates(possibleAttackers);
		}
	}

	private char[] pickRandomAttackDestination(char[] start, Board board) {
		ArrayList<char[]> possibleEnemies = new ArrayList<char[]>();
		if (this.getPieceType() == Piece.Type.BLACK) {
			if (!(board.getField((char) (start[0] - 1), (char) (start[1] - 1)).checkIfEmpty())) {

				if ((board.getField((char) (start[0] - 1), (char) (start[1] - 1)).getPiece().getPieceType() != this
						.getPieceType())
						&& (board.getField((char) (start[0] - 1), (char) (start[1] - 1)).getPiece()
								.getPieceType() != Piece.Type.BORDER)) {
					char[] diagLeftCoords = new char[] { (char) (start[0] - 1), (char) (start[1] - 1) };
					possibleEnemies.add(diagLeftCoords);
				}
			}
			if (!(board.getField((char) (start[0] + 1), (char) (start[1] - 1)).checkIfEmpty())) {
				if ((board.getField((char) (start[0] + 1), (char) (start[1] - 1)).getPiece().getPieceType() != this
						.getPieceType())
						&& (board.getField((char) (start[0] + 1), (char) (start[1] - 1)).getPiece()
								.getPieceType() != Piece.Type.BORDER)) {
					char[] diagRightCoords = new char[] { (char) (start[0] + 1), (char) (start[1] - 1) };
					possibleEnemies.add(diagRightCoords);
				}
			}
		} else {
			if (!(board.getField((char) (start[0] - 1), (char) (start[1] + 1)).checkIfEmpty())) {

				if ((board.getField((char) (start[0] - 1), (char) (start[1] + 1)).getPiece().getPieceType() != this
						.getPieceType())
						&& (board.getField((char) (start[0] - 1), (char) (start[1] + 1)).getPiece()
								.getPieceType() != Piece.Type.BORDER)) {
					char[] diagLeftCoords = new char[] { (char) (start[0] - 1), (char) (start[1] + 1) };
					possibleEnemies.add(diagLeftCoords);
				}
			}
			if (!(board.getField((char) (start[0] + 1), (char) (start[1] + 1)).checkIfEmpty())) {
				if ((board.getField((char) (start[0] + 1), (char) (start[1] + 1)).getPiece().getPieceType() != this
						.getPieceType())
						&& (board.getField((char) (start[0] + 1), (char) (start[1] + 1)).getPiece()
								.getPieceType() != Piece.Type.BORDER)) {
					char[] diagRightCoords = new char[] { (char) (start[0] + 1), (char) (start[1] + 1) };
					possibleEnemies.add(diagRightCoords);
				}
			}
		}
		return pickRandomCoordinates(possibleEnemies);
	}

	void listPossibleAttackers(Board board) {
		this.possibleAttackers = new ArrayList<char[]>();
		if (this.getPieceType() == Piece.Type.BLACK) {
			for (char[] current : getOwnPieces()) {
				if (!board.getField((char) (current[0] - 1), (char) (current[1] - 1)).checkIfEmpty()) {
					if ((board.getField((char) (current[0] - 1), (char) (current[1] - 1)).getPiece()
							.getPieceType() != this.getPieceType())
							&& (board.getField((char) (current[0] - 1), (char) (current[1] - 1)).getPiece()
									.getPieceType() != Piece.Type.BORDER)) {
						possibleAttackers.add(current);
						break;
					}
				}
				if (!board.getField((char) (current[0] + 1), (char) (current[1] - 1)).checkIfEmpty()) {
					if ((board.getField((char) (current[0] + 1), (char) (current[1] - 1)).getPiece()
							.getPieceType() != this.getPieceType())
							&& (board.getField((char) (current[0] + 1), (char) (current[1] - 1)).getPiece()
									.getPieceType() != Piece.Type.BORDER)) {
						possibleAttackers.add(current);
						break;
					}
				}
			}
		} else {
			for (char[] current : getOwnPieces()) {
				if (!board.getField((char) (current[0] - 1), (char) (current[1] + 1)).checkIfEmpty()) {
					if ((board.getField((char) (current[0] - 1), (char) (current[1] + 1)).getPiece()
							.getPieceType() != this.getPieceType())
							&& (board.getField((char) (current[0] - 1), (char) (current[1] + 1)).getPiece()
									.getPieceType() != Piece.Type.BORDER)) {
						possibleAttackers.add(current);
						break;
					}
				}
				if (!board.getField((char) (current[0] + 1), (char) (current[1] + 1)).checkIfEmpty()) {
					if ((board.getField((char) (current[0] + 1), (char) (current[1] + 1)).getPiece()
							.getPieceType() != this.getPieceType())
							&& (board.getField((char) (current[0] + 1), (char) (current[1] + 1)).getPiece()
									.getPieceType() != Piece.Type.BORDER)) {
						possibleAttackers.add(current);
						break;
					}
				}
			}
		}
	}
}
