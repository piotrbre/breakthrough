package game.players;

import java.util.ArrayList;
import java.util.Random;

import game.board.Board;
import game.board.pieces.Piece.Type;
import game.mechanics.MoveManager;

public abstract class AIPlayer extends Player {

	public AIPlayer(String playersName, Type pieceType) {
		super(playersName, pieceType);
		// TODO Auto-generated constructor stub
	}

	public void playOwnTurn(Board board, MoveManager moveManager) {
		listOwnPieces(board);
		listOwnMovablePieces(board);
		char[] start = pickStart(board);
		this.destination = pickDestination(start, moveManager, board);
		moveManager.attemptMove(start, destination, getPieceType(), board, this);
	}

	private char[] destination;

	public char[] getDestination() {
		return destination;
	}

	public void setDestination(char[] destination) {
		this.destination = destination;
	}

	public abstract char[] pickStart(Board board);

	public abstract char[] pickDestination(char[] start, MoveManager moveManager, Board board);

	/*
	 * Choses random from a list using Random
	 * 
	 * @Param ArrayList<char[]> - ArrayList of coordinates, from which random
	 * should be picked
	 */
	public static char[] pickRandomCoordinates(ArrayList<char[]> arrayList) {
		if (arrayList.size() == 0) {
			System.out.println("Ha!");
		}
		int randomIndex = new Random().nextInt(arrayList.size());
		return arrayList.get(randomIndex);
	}

	char[] pickRandomDestination(char[] start, MoveManager moveManager, Board board) {
		ArrayList<char[]> argument = new ArrayList<char[]>();
		argument = moveManager.mapPossibleDestinations(start, this.getPieceType(), board);
		return pickRandomCoordinates(argument);
	}

}
