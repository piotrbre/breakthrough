package game.players;

import java.util.ArrayList;

import game.board.Board;
import game.board.pieces.Piece;
import game.board.pieces.Piece.Type;
import game.mechanics.MoveManager;

public abstract class Player {

	private ArrayList<char[]> ownPieces;
	private ArrayList<char[]> ownMovablePieces;

	public ArrayList<char[]> getOwnPieces() {
		return ownPieces;
	}

	public ArrayList<char[]> getOwnMovablePieces() {
		return ownMovablePieces;
	}

	public Player(String playersName, Piece.Type pieceType) {
		this.playersName = playersName;
		this.pieceType = pieceType;
		this.ownPieces = new ArrayList<char[]>();
		this.ownMovablePieces = new ArrayList<char[]>();
	}

	private Type pieceType;
	private String playersName;

	public Type getPieceType() {
		return this.pieceType;
	}

	public String getName() {
		return this.playersName;
	}

	public abstract void playOwnTurn(Board board, MoveManager moveManager);

	/*
	 * Updates arraylist of own pieces
	 */
	public void listOwnPieces(Board board) {
		this.ownPieces = null;
		this.ownPieces = new ArrayList<char[]>();
		for (char x = 'a'; x <= 'h'; x++) {
			for (char y = '1'; y <= '9'; y++)
				if (!(board.getField(x, y).checkIfEmpty())) {
					if (board.getField(x, y).getPiece().getPieceType() == this.getPieceType()) {
						char[] coords = { x, y };
						this.ownPieces.add(coords);
					}
				}
		}
	}

	/*
	 * Updates arrayList of own movable pieces
	 */
	public void listOwnMovablePieces(Board board) {
		this.ownMovablePieces = null;
		this.ownMovablePieces = new ArrayList<char[]>();
		if (this.getPieceType() == Piece.Type.BLACK) {
			for (char[] current : ownPieces) {
				if (board.getField((char) (current[0] - 1), (char) (current[1] - 1)).checkIfEmpty()) {
					this.ownMovablePieces.add(current);
					continue;
				} else if (board.getField((char) (current[0]), (char) (current[1] - 1)).checkIfEmpty()) {
					this.ownMovablePieces.add(current);
					continue;
				} else if (board.getField((char) (current[0] + 1), (char) (current[1] - 1)).checkIfEmpty()) {
					this.ownMovablePieces.add(current);
					continue;
				} else {
					if ((board.getField((char) (current[0] - 1), (char) (current[1] - 1)).getPiece()
							.getPieceType() != this.getPieceType())
							&& (board.getField((char) (current[0] - 1), (char) (current[1] - 1)).getPiece()
									.getPieceType() != Piece.Type.BORDER)) {
						this.ownMovablePieces.add(current);
						continue;
					} else if ((board.getField((char) (current[0] + 1), (char) (current[1] - 1)).getPiece()
							.getPieceType() != this.getPieceType())
							&& (board.getField((char) (current[0] + 1), (char) (current[1] - 1)).getPiece()
									.getPieceType() != Piece.Type.BORDER)) {
						this.ownMovablePieces.add(current);
						continue;
					}
				}
			}
		} else {
			for (char[] current : ownPieces) {
				if (board.getField((char) (current[0] - 1), (char) (current[1] + 1)).checkIfEmpty()) {
					this.ownMovablePieces.add(current);
					continue;
				} else if (board.getField((char) (current[0]), (char) (current[1] + 1)).checkIfEmpty()) {
					this.ownMovablePieces.add(current);
					continue;
				} else if (board.getField((char) (current[0] + 1), (char) (current[1] + 1)).checkIfEmpty()) {
					this.ownMovablePieces.add(current);
					continue;
				} else {
					if ((board.getField((char) (current[0] - 1), (char) (current[1] + 1)).getPiece()
							.getPieceType() != this.getPieceType())
							&& (board.getField((char) (current[0] - 1), (char) (current[1] + 1)).getPiece()
									.getPieceType() != Piece.Type.BORDER)) {
						this.ownMovablePieces.add(current);
						continue;
					} else if ((board.getField((char) (current[0] + 1), (char) (current[1] + 1)).getPiece()
							.getPieceType() != this.getPieceType())
							&& (board.getField((char) (current[0] + 1), (char) (current[1] + 1)).getPiece()
									.getPieceType() != Piece.Type.BORDER)) {
						this.ownMovablePieces.add(current);
						continue;
					}
				}
			}
		}
	}

}
