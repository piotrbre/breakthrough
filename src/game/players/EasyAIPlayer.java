package game.players;

import game.board.Board;
import game.board.pieces.Piece.Type;
import game.mechanics.MoveManager;

public class EasyAIPlayer extends AIPlayer {

	public EasyAIPlayer(String playersName, Type pieceType) {
		super(playersName, pieceType);
		// TODO Auto-generated constructor stub
	}

	@Override
	public char[] pickDestination(char[] start, MoveManager moveManager, Board board) {
		return pickRandomDestination(start, moveManager, board);
	}

	@Override
	public char[] pickStart(Board board) {
		return pickRandomCoordinates(getOwnMovablePieces());
	}

	/*
	 * Picks random field within move range of the "start" (piece to be moved).
	 * 
	 * @Param start - coordinates of piece to be moved
	 */

}
