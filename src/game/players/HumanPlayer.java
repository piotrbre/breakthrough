package game.players;

import java.util.Scanner;

import game.board.Board;
import game.board.pieces.Piece.Type;
import game.mechanics.MoveManager;

public class HumanPlayer extends Player {

	public HumanPlayer(String playersName, Type pieceType) {
		super(playersName, pieceType);
		// TODO Auto-generated constructor stub
	}

	Scanner scanner = new Scanner(System.in);

	@Override
	public void playOwnTurn(Board board, MoveManager moveManager) {
		char[] ch = getMoveCoordinates(moveManager);
		char[] start = { ch[0], ch[1] };
		char[] destination = { ch[3], ch[4] };
		while (!moveManager.attemptMove(start, destination, this.getPieceType(), board, this)) {
			ch = getMoveCoordinates(moveManager);
			start = new char[] { ch[0], ch[1] };
			destination = new char[] { ch[3], ch[4] };
		}
	}

	private char[] getMoveCoordinates(MoveManager moveManager) {
		char[] ch = { '0', '0', '0', '0', '0' };
		String line;
		do {
			System.out.println();
			System.out.println("Podaj prawidlowe wspolrzedne: ");
			line = scanner.nextLine();
			if (line.length() >= 5) {
				ch[0] = line.charAt(0);
				ch[1] = line.charAt(1);
				ch[2] = ' ';
				ch[3] = line.charAt(3);
				ch[4] = line.charAt(4);
			} else {
				continue;
			}
		} while ((!moveManager.areCoords(ch, 0, 1) && !moveManager.areCoords(ch, 3, 4)));
		return ch;
	}
}
