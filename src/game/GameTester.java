package game;

import game.board.pieces.Piece;
import game.players.EasyAIPlayer;
import game.players.MediumAIPlayer;

public class GameTester {
	/*
	 * Test function. Configure game by changing static with one of following
	 * Player Subclasses: EasyAIPlayer MediumAIPlayer HumanPlayer
	 * 
	 * Game board legend:
	 * 
	 * @ - white pieces. Must be PLAYER1's pieces. Spawned on the bottom of the
	 * board (rows 1 and 2)
	 * 
	 * # - black pieces. Must be PLAYER2's pieces. Spawned on top of the map
	 * (rows 7 and 8)
	 * 
	 * . - border of map
	 * 
	 * coordinates of the board:
	 * 
	 * 8
	 * 
	 * 7
	 * 
	 * 6
	 * 
	 * 5
	 * 
	 * 4
	 * 
	 * 3
	 * 
	 * 2
	 * 
	 * 1
	 * 
	 * a b c d e f g h
	 * 
	 */

	private static MediumAIPlayer PLAYER1 = new MediumAIPlayer("SrednioTrudny", Piece.Type.WHITE);
	private static EasyAIPlayer PLAYER2 = new EasyAIPlayer("Latwy", Piece.Type.BLACK);

	public static void main(String[] args) {

		Game newGame = new Game(PLAYER1, PLAYER2);
		newGame.initialize();
		newGame.goOn();
	}

}
